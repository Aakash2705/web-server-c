#!/bin/sh
set -xe

mkdir -p build/
gcc -o build/server server.c -Wall -Wextra -pedantic
gcc -o build/client client.c -Wall -Wextra -pedantic
echo "Compilation finished"
