#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>

#define PORT 8080

int main (int argc, char **argv)
{
  if (argc == 0) {fprintf(stderr, "Not enough arguments specified\n"); return -69;}
  int sock = 0;
  long valread;

  struct sockaddr_in serv_addr;
  char *hello = argv[1];
  char buffer[1024] = {0};

  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    fprintf(stderr, "COULD NOT CREATE SOCKET :(\n");
    return -1;
  }

  memset(&serv_addr, '0', sizeof(serv_addr));
    
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(PORT);
    
    // Convert IPv4 and IPv6 addresses from text to binary form
  if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
      printf("\nInvalid address/ Address not supported \n");
      return -1;
  }
    
  if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
  {
      fprintf(stderr, "CONNECTION FAILED :(");
      return -1;
  }
  send(sock , hello , strlen(hello) , 0 );
  printf("Hello message sent\n");
  valread = read( sock , buffer, 1024);
  printf("%s\n",buffer );
  return 0;
}
