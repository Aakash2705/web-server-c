#include <unistd.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <netinet/in.h>

const int PORT = 8080;
char *msg = "Hello from server";

int main() {
  int server_fd, new_socket;
  if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    fprintf(stderr, "Cannot create socket :(\n");
    return -1;
  }
  
  struct sockaddr_in address;
  int addrlen = sizeof(address);
  memset((char*)&address, 0, sizeof(address));
  address.sin_addr.s_addr = htonl(INADDR_ANY);
  address.sin_family = AF_INET;
  address.sin_port = htons(PORT);

  if (bind(server_fd,(struct sockaddr *)&address,sizeof(address)) < 0)  { 
    fprintf(stderr,"could not bind socket :(\n"); 
    return -2; 
  }

  if (listen(server_fd, 6) < 0) {
    fprintf(stderr, "could not listen to server_fd\n");
    return -3;
  }
  while(1) {
    if((new_socket = accept(server_fd, (struct sockaddr*)&address, (socklen_t*)&addrlen))<0) {fprintf(stderr, "Error accepting socket :(\n"); return -4;}
    
    char buffer[4096] = {0};
    long valread = read(new_socket, buffer, sizeof(buffer));
    if (strcmp(buffer, "close") == 0) {close(server_fd); printf("Connection closed\n"); return 0;}
    printf("%s\n", buffer);

    write(new_socket , msg , strlen(msg));
    printf("------------------Hello message sent-------------------\n");
    close(new_socket);
  }
  return 0;
}

